-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2018 at 02:41 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maba_kopertip`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar`
--

CREATE TABLE `daftar` (
  `id` int(11) NOT NULL,
  `nm_lkp` varchar(50) NOT NULL,
  `jns_klmn` varchar(15) NOT NULL,
  `tmpt_lhr` varchar(50) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `almt_rmh` text NOT NULL,
  `kab_kota` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `no_telp` int(20) NOT NULL,
  `email` varchar(75) NOT NULL,
  `thn_brngkt` date NOT NULL,
  `rekomendasi` varchar(100) NOT NULL,
  `asal_sklh` varchar(50) NOT NULL,
  `nama_sekolah` varchar(75) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `thn_lulus` date NOT NULL,
  `kab_kota1` varchar(50) NOT NULL,
  `nm_perguruan_tinggi` varchar(100) NOT NULL,
  `program_studi` varchar(75) NOT NULL,
  `kab_kota2` varchar(75) NOT NULL,
  `tuj_neg_ped` varchar(10) NOT NULL,
  `jjg_pnd` varchar(10) NOT NULL,
  `perguruan_tinggi_china` varchar(100) NOT NULL,
  `jurusan_1` varchar(100) NOT NULL,
  `nama_wali` varchar(100) NOT NULL,
  `status_wali` varchar(10) NOT NULL,
  `almt_rmh1` text NOT NULL,
  `jenis_kerja` varchar(50) NOT NULL,
  `jabatan_kerja` varchar(50) NOT NULL,
  `penghasilan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar`
--
ALTER TABLE `daftar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar`
--
ALTER TABLE `daftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
