-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2018 at 05:27 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maba_kopertip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_daftar`
--

CREATE TABLE `tb_daftar` (
  `id` int(11) NOT NULL,
  `nm_lkp` varchar(100) NOT NULL,
  `jns_klmn` varchar(50) NOT NULL,
  `tmpt_lhr` varchar(100) NOT NULL,
  `tgl_lhr` varchar(10) NOT NULL,
  `almt_rmh` text NOT NULL,
  `kab_kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `no_telp` int(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `thn_brngkt` int(5) NOT NULL,
  `rekomendasi` text NOT NULL,
  `asal_sklh` varchar(100) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `thn_lulus` date NOT NULL,
  `kab_kota1` varchar(100) NOT NULL,
  `nm_prguruan_tinggi` varchar(100) NOT NULL,
  `prgrm_stdi` varchar(100) NOT NULL,
  `kab_kota2` varchar(100) NOT NULL,
  `perguruan_tinggi_china` varchar(100) NOT NULL,
  `jurusan_1` varchar(100) NOT NULL,
  `nama_wali` varchar(100) NOT NULL,
  `status_wali` varchar(50) NOT NULL,
  `almt_rmh1` text NOT NULL,
  `jenis_kerja` varchar(100) NOT NULL,
  `jabatan_kerja` varchar(100) NOT NULL,
  `penghasilan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_daftar`
--

INSERT INTO `tb_daftar` (`id`, `nm_lkp`, `jns_klmn`, `tmpt_lhr`, `tgl_lhr`, `almt_rmh`, `kab_kota`, `provinsi`, `no_telp`, `email`, `thn_brngkt`, `rekomendasi`, `asal_sklh`, `jurusan`, `thn_lulus`, `kab_kota1`, `nm_prguruan_tinggi`, `prgrm_stdi`, `kab_kota2`, `perguruan_tinggi_china`, `jurusan_1`, `nama_wali`, `status_wali`, `almt_rmh1`, `jenis_kerja`, `jabatan_kerja`, `penghasilan`) VALUES
(4, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', 'tgl_lhr', 'almt_rmh', 'kab_kota', 'provinsi', 0, 'email', 0, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(5, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', '2018-01-04', 'Jl.Rancakasiat', 'Bandung', 'Jawa Barat', 2222222, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(6, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', '2018-01-04', 'Jl.Rancakasiat', 'Bandung', 'Jawa Barat', 2222222, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(7, 'Ihsan Karunia MIndara', 'laki-laki', 'tmpt_lhr', '2018-01-10', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2019, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(8, 'Ihsan Karunia MIndara', 'laki-laki', 'B', '', '', '', '', 0, '', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(9, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(10, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-10', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(11, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', ' Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 89223, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(12, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', ' Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 89223, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0),
(13, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018, '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_daftar`
--
ALTER TABLE `tb_daftar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_daftar`
--
ALTER TABLE `tb_daftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
