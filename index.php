<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>BEASISWA KOPERTIP ACADEMIC & LEADERSHIP PROGRAM 2018 CHINA</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"><link rel="stylesheet" href="css/style.css">
</head>
<body>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sign Up Form</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <div>
    <?php 
    if(isset($_GET['err1'])){
      echo '<div class="alert-error">Maaf, nomor telepon harus angka!!!</div>';
    }
    ?>
      <form action="c.php" method="post">
      
        <h1>BEASISWA KOPERTIP ACADEMIC & LEADERSHIP PROGRAM 2018 CHINA</h1>
        
        <fieldset>
          <legend><span class="number">1</span>Biodata</legend>
          <label>Nama Lengkap :</label>
          <input type="text" name="nm_lkp">

          <label>Jenis Kelamin :</label>
          <select name="jns_klmn">
            <option value="laki-laki">Laki - laki</option>
            <option value="perempuan">Perempuan</option>
          </select>
          
          <label>Tempat Lahir :</label>
          <input type="text" name="tmpt_lhr">
          
          <label>Tanggal Lahir :</label>
          <input type="date" name="tgl_lhr">

          <label>Alamat Rumah :</label>
          <textarea name="almt_rmh" rows="5" cols="50"></textarea>

          <label>Kabupaten / Kota :</label>
          <input type="text" name="kab_kota">

          <label>Provinsi :</label>
          <input type="text" name="provinsi">

          <label>No.Telp / Handphone :</label>
          <input type="text" name="no_telp">

          <label>Email :</label>
          <input type="email" name="email">

          <label>Tahun Keberangkatan :</label>
          <select name="thn_brngkt">
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
          </select>
        </fieldset>

        <fieldset>
          <legend><span class="number">2</span>Rekomendasi</legend>
          <label>Dari mana Anda mendapatkan Informasi/ rekomendasi kuliah ke China? (Sebutkan nama atau Lembaga)</label>
          <input type="text" name="rekomendasi">
        </fieldset>

        <fieldset>
          <legend><span class="number">3</span>Data Sekolah</legend>
            <font size="2" color="gray"><i>Diisi untuk lulusan SMA</i></font>
          <br><br><label>Asal Sekolah :</label>
          <input type="text" name="asal_sklh">

          <label>Nama Sekolah :</label>
          <input type="text" name="nama_sekolah">


          <label>Jurusan / Paket Pilihan :</label>
          <input type="text" name="jurusan">

          <label>Tahun Kelulusan :</label>
          <input type="text" name="thn_lulus">

          <label>Kabupaten / Kota</label>
          <input type="text" name="kab_kota1">
        </fieldset>

        <fieldset>
          <legend><span class="number">4</span>Data Perguruan Tinggi</legend>
          <font size="2" color="gray">Diisi untuk Mahasiswa yang melakukan Transfer Kredit.</font>
          <br><br><label>Nama Perguruan Tinggi :</label>
          <input type="text" name="nm_prguruan_tinggi">

          <label>Program Studi :</label>
          <input type="text" name="program_studi">

          <label>Kabupaten / Kota</label>
          <input type="text" name="kab_kota2">
        </fieldset>

        <fieldset>
            <legend><span class="number">5</span>Pilihan Perguruan Tinggi</legend>
              <font size="2" color="gray">Untuk tahun 2017 hanya Universitas di China saja.</font>
            <br><br><label>Tujuan Negara Pendidikan :</label>
            <select name="tuj_neg_ped">
              <option value="china">China</option>
            </select> 

            <br><br><label>Jenjang Pendidikan :</label>
            <select name="jjg_pnd">
              <option value="d3">D3</option>
              <option value="s1">S1</option>
            </select>

            <label>Pilihan Perguruan Tinggi China</label>
            <select name="perguruan_tinggi_china">
              <option value="Wuxi Institute of Technology (WXIT)">Wuxi Institute of Technology (WXIT)</option>
              <option value="Changzhou Institute of Mechatronic Technology (CZIMT)">Changzhou Institute of Mechatronic Technology (CZIMT)</option>
              <option value="Nanjing College of Information Technology (NJCIT)">Nanjing College of Information Technology (NJCIT)</option>
              <option value="Nanjing Institute of Railway Technology (NIRT)">Nanjing Institute of Railway Technology (NIRT)</option>
              <option value="Nanjing Polytechnic of Insitute (NJPI)">Nanjing Polytechnic of Insitute (NJPI)</option>
              <option value="Nanjing University of Information Science and Technology (NUIST)">Nanjing University of Information Science and Technology (NUIST)</option>
              <option value="Yangzhou Polytechnic of Insitute (YPI)">Yangzhou Polytechnic of Insitute (YPI)</option>
              <option value="Yangzhou Polytechnic College">Yangzhou Polytechnic College</option>
              <option value="Jiangsu Vocational College of Medicine(JSMC)">Jiangsu Vocational College of Medicine(JSMC)</option>
              <option value="Yellow River Concervancy Technical Institute (YRCTI)">Yellow River Concervancy Technical Institute (YRCTI)</option>
              <option value="Hainan Medical University (HNMU)">Hainan Medical University (HNMU)</option>
            </select>

            <font size="" ="2" color="gray">Pilihlah salah satu Program Studi sesuai dengan pilihan Perguruan Tinggi masing-masing.</font>
            <br><br><label>Program Studi :</label>
            <select name="jurusan_1">
              <optgroup label="Wuxi Institute of Technology (WXIT)">
                    <option value="Mechanical Manufacture and Automation">Mechanical Manufacture and Automation</option>
                    <option value="Mechatronic Technology">Mechatronic Technology</option>
                    <option value="Software Technology">Software Technology</option>
                    <option value="Applied Electronic Technology">Applied Electronic Technology</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Logistic Management">Logistic Management</option>
                    <option value="International Business (Sino-Australia Joint)">International Business (Sino-Australia Joint)</option>
                    <option value="Automobile Testing & Manitenance Technology">Automobile Testing & Manitenance Technology</option>
                    <option value="Advertising Design dan Fracture">Advertising Design dan Fracture</option>   
              </optgroup>
              <optgroup label="Changzhou Institute of Mechatronic Technology (CZIMT)">
                    <option value="School of Mechanical Engineering">School of Mechanical Engineering</option>
                    <option value="School of Die & Mold Technology">School of Die & Mold Technology</option>
                    <option value="School of Vehicle Engineering">School of Vehicle Engineering</option>
                    <option value="School of Information Engineering">School of Information Engineering</option>
                    <option value="School of Economics and Management">School of Economics and Management</option>
                    <option value="School of Art and Design">School of Art and Design</option>
              </optgroup>
              <optgroup label="Nanjing College of Information Technology (NJCIT)">
                    <option value="Communication Technology">Communication Technology</option>
                    <option value="Electronic Information Technology">Electronic Information Technology</option>
                    <option value="Mechatronics Technology">Mechatronics Technology</option>
                    <option value="Chain Store Management">Chain Store Management</option>
                    <option value="Digital Media Design">Digital Media Design</option>
              </optgroup>
              <optgroup label="Nanjing Institute of Railway Technology (NIRT)">
                <option value="Technology of Railway Engineering">Technology of Railway Engineering</option>
                <option value="Technology of electrified Railway">Technology of electrified Railway</option>
                <option value="Railway Locomotive and Vehicle">Railway Locomotive and Vehicle</option>
                <option value="Operation and Management of Railway Transportation">Operation and Management of Railway Transportation</option>
                <option value="Environmental Arts and Design">Environmental Arts and Design</option>
                <option value="Tourism Management">Tourism Management</option>
                <option value="International Business">International Business</option>
                <option value="Railway Communication Signal">Railway Communication Signal</option>
              </optgroup>
              <optgroup label="Nanjing Polytechnic of Insitute (NJPI)">
                <option value="Chinese Languange">Chinese Languange</option> 
                <option value="Applied Chemical Technology">Applied Chemical Technology</option> 
                <option value="Environment Engineering Technology">Environment Engineering Technology</option> 
                <option value="Industrial Analysis Technology">Industrial Analysis Technology</option> 
                <option value="Software Engineering">Software Engineering</option> 
                <option value="Mechanical Manufacture & Automation">Mechanical Manufacture & Automation</option>
                <option value="Electric Power Plant of Thermal Power Instalation">Electric Power Plant of Thermal Power Instalation</option> 
                <option value="Electrical Automation Technology">Electrical Automation Technology</option> 
                <option value="International Business">International Business</option>
                <option value="Computer Numerical  Control (CNC)">Computer Numerical  Control (CNC)</option>
                <option value="Mechatronics Technology">Mechatronics Technology</option>
              </optgroup>
            <optgroup label="Nanjing University of Information Science and Technology (NUIST)">
                <option value="International Economy and Trade">International Economy and Trade</option>
                <option value="Electronic Information Engineering">Electronic Information Engineering</option>
            </optgroup>
            <optgroup label="Yangzhou Polytechnic of Institute (YPI)">
                <option value=""></option>
                <option value=""></option>
            </optgroup>
            <optgroup label="Yangzhou Polytechnic College">
                <option value="Teknik">Teknik</option>
            </optgroup>
            <optgroup label="">
                <option value=""></option>
            </optgroup>
            </select>
        </fieldset>

        <fieldset>
            <legend><span class="number">6</span>Data Penjamin Pendidikan</legend>
            <font color="gray" size="2">Data diri Orang Tua / Wali yang menjamin Anda Kuliah</font>
            <br><br><label>Nama :</label>
            <input type="text" name="nama_wali">

            <label>Status :</label>
            <select name="status_wali">
              <option value="ayah">Ayah</option>
              <option value="ibu">Ibu</option>
              <option value="wali">Wali</option>
            </select>

            <label>Alamat Rumah :</label>
            <textarea name="almt_rmh1" rows="5" cols="50"></textarea>

            <label>Jenis Pekerjaan :</label>
            <input type="text" name="jenis_kerja">

            <label>Jabatan :</label>
            <input type="text" name="jabatan_kerja">

            <label>Penghasilan :</label>
            <select name="penghasilan">
                <option value="5jt">< 5 Juta</option>
                <option value="5-10jt"> 5-10 Juta</option>
                <option value="10_jt">> 10 Juta</option>
            </select>
        </fieldset>

        <fieldset>
            <font size="4"><b><legend><span class="text"><u>INFO</span></u></legend></b></font>
            <font size="3" color="gray">Setelah mendaftar harap kirimkan BIODATA DIRI dengan format bebas dalam bahasa Indonesia. Bagi yang memilih Universitas Nanjing Polytechnic of Institute kirimkan BIODATA DIRI  dalam bentuk bahasa Inggris.
Kirimkan ke alamat email : <font color="blue"><u>china@kopertipindonesia.or.id</u></font> dan <font color="blue"><u> china.kopertipindonesia@gmail.com</u></font> (sertakan kode Perguruan Tinggi *Contoh:WXIT) </font>
        </fieldset>
        <button type="submit" name="daftar">Daftar</button>
      </form>
      </div>
    </body>
</html>
  
  
</body>
</html>
