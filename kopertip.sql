-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05 Jan 2018 pada 09.54
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kopertip`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_daftar`
--

CREATE TABLE `tb_daftar` (
  `id` int(11) NOT NULL,
  `nm_lkp` varchar(100) NOT NULL,
  `jns_klmn` varchar(50) NOT NULL,
  `tmpt_lhr` varchar(100) NOT NULL,
  `tgl_lhr` varchar(10) NOT NULL,
  `almt_rmh` text NOT NULL,
  `kab_kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `no_telp` int(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `thn_brngkt` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_daftar`
--

INSERT INTO `tb_daftar` (`id`, `nm_lkp`, `jns_klmn`, `tmpt_lhr`, `tgl_lhr`, `almt_rmh`, `kab_kota`, `provinsi`, `no_telp`, `email`, `thn_brngkt`) VALUES
(4, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', 'tgl_lhr', 'almt_rmh', 'kab_kota', 'provinsi', 0, 'email', 0),
(5, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', '2018-01-04', 'Jl.Rancakasiat', 'Bandung', 'Jawa Barat', 2222222, 'ihsankarunia09@gmail.com', 2018),
(6, 'Cintami Fatmajaya', 'perempuan', 'tmpt_lhr', '2018-01-04', 'Jl.Rancakasiat', 'Bandung', 'Jawa Barat', 2222222, 'ihsankarunia09@gmail.com', 2018),
(7, 'Ihsan Karunia MIndara', 'laki-laki', 'tmpt_lhr', '2018-01-10', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2019),
(8, 'Ihsan Karunia MIndara', 'laki-laki', 'B', '', '', '', '', 0, '', 2018),
(9, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018),
(10, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-10', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018),
(11, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', ' Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 89223, 'ihsankarunia09@gmail.com', 2018),
(12, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', ' Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 89223, 'ihsankarunia09@gmail.com', 2018),
(13, 'Ihsan Karunia MIndara', 'laki-laki', 'Bandung', '2018-01-03', 'Jl.Rancakasiat', 'Kota Bandung', 'Jawa Barat', 2147483647, 'ihsankarunia09@gmail.com', 2018);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_daftar`
--
ALTER TABLE `tb_daftar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_daftar`
--
ALTER TABLE `tb_daftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
